Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'profile/valentinaloreto' => 'profiles#json_response'

  get 'browse', to: 'browse#search', as: :browse  # 1 -renders the search view bar. linked to friends/new

  post 'friends/new', to: 'friends#new'
  post 'friends', to: 'friends#create' #f 3 ----from hidden form
  get '/friends/:id', to: 'friends#show', as: 'friend'

  resources :profiles, only: [:show, :edit, :update], shallow: true do
    resources :hobbies, except: :show
    resources :photos
    resources :certificates, except: :show
  end
end
