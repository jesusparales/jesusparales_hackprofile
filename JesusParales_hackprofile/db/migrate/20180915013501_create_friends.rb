class CreateFriends < ActiveRecord::Migration[5.2]
  def change
    create_table :friends do |t|
      t.string :username
      t.string :url
      t.references :profile, foreign_key: true

      t.timestamps
    end
  end
end
