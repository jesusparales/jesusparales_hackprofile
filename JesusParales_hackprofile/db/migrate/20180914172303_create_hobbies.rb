class CreateHobbies < ActiveRecord::Migration[5.2]
  def change
    create_table :hobbies do |t|
      t.string :name
      t.text :description
      t.integer :preference
      t.references :profile, foreign_key: true
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
