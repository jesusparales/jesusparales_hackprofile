# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

categories = ['Musica','Lectoescritura','Aventura', 'Fitness', 'Coleccionismo', 'Entretenimiento', 'Construccion', 'Gastronomia']

categories.each do |category|
  Category.create({
    name: category
  })
end

user = User.create({
  email: 'vloretocastillo@gmail.com',
  password: 'pass'
})

profile = Profile.create({
  firstname: 'Valentina',
  lastname: 'Loreto',
  subtitle: 'Web Developer',
  username: 'Githugs',
  bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  birthdate: Date.parse('1991-06-29'),
  user_id: user.id
})

4.times do |i|
  Hobby.create({
    name: 'Reading',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris',
    preference: 5,
    profile_id: profile.id ,
    category_id: 2
  })
end
