class FriendsController < ApplicationController
  before_action :set_profile
  skip_before_action :verify_authenticity_token

  def new
    @friend = Friend.new
    @url = params[:url]
    @response = HTTParty.get(@url)
    @age = set_age(@response['birthdate'].to_time)
  end

  def create
    @url = params[:url]
    #render json: @url
     @friend = @profile.friends.build({
       username: 'friend2',
       url: @url,
       profile_id: @profile.id
     })
     if @friend.save
       redirect_to profile_path(@profile), notice: 'Friend was successfully created.'
     else
       render :new
     end
  end

  def show
    friend = Friend.find(params[:id])
    @response = HTTParty.get(friend.url)
    @age = set_age(@response['birthdate'].to_time)
  end

  private

  def set_profile
    @profile = Profile.find(1)
  end

  def set_age b
    now = Time.now
    now.year - b.year - ((now.month > b.month || (now.month == b.month && now.day >= b.day)) ? 0 : 1)
  end

end
