class Hobby < ApplicationRecord
  belongs_to :profile
  belongs_to :category

  #enum preference: [:one, :two, :three, :four, :five]
  enum preference: {
    why_not: 1,
    its_ok: 2,
    like_it: 3,
    love_it: 4,
    favorite: 5
  }

end
